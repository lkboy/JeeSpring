/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.oa.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.oa.entity.OaNotify;

/**
 * 通知通告DAO接口
 * @author jeertd
 * @version 2014-05-16
 */
@RtdMyBatisDao
public interface OaNotifyDao extends RtdRepositoryDao<OaNotify> {
	
	/**
	 * 获取通知数目
	 * @param oaNotify
	 * @return
	 */
	public Long findCount(OaNotify oaNotify);
	
}
/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.tools.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.tools.entity.TestInterface;

/**
 * 接口DAO接口
 * @author lgf
 * @version 2016-01-07
 */
@RtdMyBatisDao
public interface TestInterfaceDao extends RtdRepositoryDao<TestInterface> {
	
}
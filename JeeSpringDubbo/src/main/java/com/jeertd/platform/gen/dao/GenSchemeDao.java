package com.jeertd.platform.gen.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.gen.entity.GenScheme;

@RtdMyBatisDao
public abstract interface GenSchemeDao
  extends RtdRepositoryDao<GenScheme>
{}

package com.jeertd.platform.gen.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.gen.entity.GenTable;

import org.apache.ibatis.annotations.Param;

@RtdMyBatisDao
public abstract interface GenTableDao
  extends RtdRepositoryDao<GenTable>
{
  public abstract int buildTable(@Param("sql") String paramString);
}

package com.jeertd.platform.gen.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.gen.entity.GenTemplate;

@RtdMyBatisDao
public abstract interface GenTemplateDao
  extends RtdRepositoryDao<GenTemplate>
{}

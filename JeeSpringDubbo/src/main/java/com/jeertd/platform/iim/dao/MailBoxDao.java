/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.iim.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.iim.entity.MailBox;

/**
 * 发件箱DAO接口
 * @author jeertd
 * @version 2015-11-15
 */
@RtdMyBatisDao
public interface MailBoxDao extends RtdRepositoryDao<MailBox> {
	
	public int getCount(MailBox entity);
	
}
/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.iim.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeertd.core.orm.RtdPage;
import com.jeertd.core.service.RtdRepositoryService;
import com.jeertd.platform.iim.dao.MailBoxDao;
import com.jeertd.platform.iim.dao.MailComposeDao;
import com.jeertd.platform.iim.entity.MailBox;
import com.jeertd.platform.iim.entity.MailCompose;
import com.jeertd.platform.iim.entity.MailPage;

/**
 * 发件箱Service
 * @author jeertd
 * @version 2015-11-13
 */
@Service
@Transactional(readOnly = true)
public class MailComposeService extends RtdRepositoryService<MailComposeDao, MailCompose> {
	@Autowired
	private MailComposeDao mailComposeDao;
	public MailCompose get(String id) {
		return super.get(id);
	}
	
	public List<MailCompose> findList(MailCompose mailCompose) {
		return super.findList(mailCompose);
	}
	
	public RtdPage<MailCompose> findPage(MailPage<MailCompose> page, MailCompose mailCompose) {
		return super.findPage(page, mailCompose);
	}
	
	@Transactional(readOnly = false)
	public void save(MailCompose mailCompose) {
		super.save(mailCompose);
	}
	
	@Transactional(readOnly = false)
	public void delete(MailCompose mailCompose) {
		super.delete(mailCompose);
	}

	public int getCount(MailCompose mailCompose) {
		return mailComposeDao.getCount(mailCompose);
	}
	
}
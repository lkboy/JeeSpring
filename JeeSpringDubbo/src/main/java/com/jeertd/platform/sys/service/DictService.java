/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeertd.core.common.utils.CacheUtils;
import com.jeertd.core.service.RtdRepositoryService;
import com.jeertd.platform.sys.dao.DictDao;
import com.jeertd.platform.sys.entity.Dict;
import com.jeertd.platform.sys.utils.DictUtils;

/**
 * 字典Service
 * @author jeertd
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class DictService extends RtdRepositoryService<DictDao, Dict> {
	
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList(){
		return dao.findTypeList(new Dict());
	}

	@Transactional(readOnly = false)
	public void save(Dict dict) {
		super.save(dict);
		CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
	}

	@Transactional(readOnly = false)
	public void delete(Dict dict) {
		super.delete(dict);
		CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
	}

}

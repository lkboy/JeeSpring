/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.sys.dao;

import com.jeertd.core.orm.RtdTreeDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.sys.entity.Area;

/**
 * 区域DAO接口
 * @author jeertd
 * @version 2014-05-16
 */
@RtdMyBatisDao
public interface AreaDao extends RtdTreeDao<Area> {
	
}

/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.test.dao.tree;

import com.jeertd.core.orm.RtdTreeDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.test.entity.tree.TestTree;

/**
 * 组织机构DAO接口
 * @author liugf
 * @version 2016-01-15
 */
@RtdMyBatisDao
public interface TestTreeDao extends RtdTreeDao<TestTree> {
	
}
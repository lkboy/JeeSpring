/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.core.orm;

import java.util.Date;

import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jeertd.core.common.utils.IdGen;
import com.jeertd.platform.sys.entity.User;
import com.jeertd.platform.sys.utils.UserUtils;

/**
 * 数据Entity类
 * @author jeertd
 * @version 2014-05-16
 */
public abstract class RtdDataEntity<T> extends RtdBaseEntity<T> {

	private static final long serialVersionUID = 1L;
	
	@QueryParam("remarks")
	@FormParam("remarks")
	protected String remarks;	// 备注
	@org.codehaus.jackson.annotate.JsonIgnore
	protected User createBy;	// 创建者
	protected Date createDate;	// 创建日期
	@org.codehaus.jackson.annotate.JsonIgnore
	protected User updateBy;	// 更新者
	protected Date updateDate;	// 更新日期
	@org.codehaus.jackson.annotate.JsonIgnore
	protected String delFlag; 	// 删除标记（0：正常；1：删除；2：审核）
	
	public RtdDataEntity() {
		super();
		this.delFlag = DEL_FLAG_NORMAL;
	}
	
	public RtdDataEntity(String id) {
		super(id);
	}
	
	/**
	 * 插入之前执行方法，需要手动调用
	 */
	@Override
	public void preInsert(){
		// 不限制ID为UUID，调用setIsNewRecord()使用自定义ID
		//if (!this.isNewRecord){
		if(StringUtils.isBlank(getId())){
			setId(IdGen.uuid());
		}
		User user = UserUtils.getUser();
		if (StringUtils.isNotBlank(user.getId())){
			this.updateBy = user;
			this.createBy = user;
		}
		this.updateDate = new Date();
		this.createDate = this.updateDate;
	}
	
	/**
	 * 更新之前执行方法，需要手动调用
	 */
	@Override
	public void preUpdate(){
		User user = UserUtils.getUser();
		if (StringUtils.isNotBlank(user.getId())){
			this.updateBy = user;
		}
		this.updateDate = new Date();
	}
	
	@Length(min=0, max=255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@JsonIgnore
	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@JsonIgnore
	public User getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(User updateBy) {
		this.updateBy = updateBy;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@JsonIgnore
	@Length(min=1, max=1)
	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

}

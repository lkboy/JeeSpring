/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.core.base.beanvalidator;

/**
 * 默认Bean验证组
 * @author jeertd
 */
public interface RtdDefaultGroup {

}

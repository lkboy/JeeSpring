﻿# Host: localhost  (Version: 5.5.47)
# Date: 2017-12-18 14:12:56
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "sys_config"
#

CREATE TABLE `sys_config` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `value` varchar(100) NOT NULL COMMENT '数据值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `description` varchar(2000) NOT NULL COMMENT '描述',
  `sort` decimal(10,0) NOT NULL COMMENT '排序（升序）',
  `parent_id` varchar(64) DEFAULT '0' COMMENT '父级编号',
  `parent_ids` varchar(2000) DEFAULT '0' COMMENT '所有父级编号',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置表';

#
# Data for table "sys_config"
#

INSERT INTO `sys_config` VALUES 
('9a9d64ff20124da191cd8c717acfbb07','toExceptionMailAddr','516821420@qq.com','异常接收邮箱地址(可以用逗号隔开发多个邮件)','异常接收邮箱地址(可以用逗号隔开发多个邮件)',111,'0','0','1','2017-12-11 11:46:25','1','2017-12-12 16:49:26','异常接收邮箱地址','0');

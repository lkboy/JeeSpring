﻿DROP TABLE IF EXISTS `monitor`;

CREATE TABLE `monitor` (
  
`id` varchar(64) NOT NULL default '' COMMENT '主键',
  
`cpu` varchar(64) default NULL COMMENT 'cpu使用率',
  
`jvm` varchar(64) default NULL COMMENT 'jvm使用率',
  
`ram` varchar(64) default NULL COMMENT '内存使用率',
  
`toemail` varchar(64) default NULL COMMENT '警告通知邮箱',
  
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='系统监控';


INSERT INTO `monitor` (`id`,`cpu`,`jvm`,`ram`,`toemail`) VALUES 
 ('1','99','99','99','516821420@qq.com');

DROP TABLE IF EXISTS `systemconfig`;
CREATE TABLE `systemconfig` (
  
`id` varchar(64) NOT NULL default '' COMMENT '主键',
  
`smtp` varchar(64) default NULL COMMENT '邮箱服务器地址',
  
`port` varchar(64) default NULL COMMENT '邮箱服务器端口',
  
`mailname` varchar(64) default NULL COMMENT '系统邮箱地址',
  
`mailpassword` varchar(64) default NULL COMMENT '系统邮箱密码',
  
`smsname` varchar(64) default NULL COMMENT '短信用户名',
  
`smspassword` varchar(64) default NULL COMMENT '短信密码',
  
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='系统配置';

/*!40000 ALTER TABLE `systemconfig` DISABLE KEYS */;

INSERT INTO `systemconfig` (`id`,`smtp`,`port`,`mailname`,`mailpassword`,`smsname`,`smspassword`) VALUES 
 
('1','smtp.qq.com','25','516821420@qq.com','xxxxxxx','110931','xxxxxx');

/*!40000 ALTER TABLE `systemconfig` ENABLE KEYS */;


INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
('f18fac5c4e6145528f3c1d87dbcb37d5','67','0,1,67,','系统监控管理','70','/monitor/info','','','1','','1','2016-02-02 22:49:07','1','2016-02-02 23:15:07','','0'),
('8e01a74a9ca94a26a5263769c354afb9','67','0,1,67,','系统配置','100','/sys/systemConfig','','','1','sys:systemConfig:index','1','2016-02-07 16:25:22','1','2016-02-07 16:25:22','','0');


--
-- Definition of table `test_line_weather_main_city`
--

DROP TABLE IF EXISTS `test_line_weather_main_city`;
CREATE TABLE `test_line_weather_main_city` (
  `id` varchar(64) NOT NULL default '' COMMENT '主键',
  `create_by` varchar(64) default NULL COMMENT '创建者',
  `create_date` datetime default NULL COMMENT '创建时间',
  `update_by` varchar(64) default NULL COMMENT '更新者',
  `update_date` datetime default NULL COMMENT '更新时间',
  `remarks` varchar(255) character set utf8 default NULL COMMENT '备注信息',
  `del_flag` varchar(64) default NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `datestr` datetime default NULL COMMENT '日期',
  `beijing_maxtemp` double default NULL COMMENT '北京最高气温',
  `beijing_mintemp` double default NULL COMMENT '北京最低气温',
  `changchun_maxtemp` double default NULL COMMENT '长春最高气温',
  `changchun_mintemp` double default NULL COMMENT '长春最低气温',
  `shenyang_maxtemp` double default NULL COMMENT '沈阳最高气温',
  `shenyang_mintemp` double default NULL COMMENT '沈阳最低气温',
  `haerbin_maxtemp` double default NULL COMMENT '哈尔滨最高气温',
  `haerbin_mintemp` double default NULL COMMENT '哈尔滨最低气温',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='城市气温';

--
-- Dumping data for table `test_line_weather_main_city`
--

/*!40000 ALTER TABLE `test_line_weather_main_city` DISABLE KEYS */;
INSERT INTO `test_line_weather_main_city` (`id`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`,`datestr`,`beijing_maxtemp`,`beijing_mintemp`,`changchun_maxtemp`,`changchun_mintemp`,`shenyang_maxtemp`,`shenyang_mintemp`,`haerbin_maxtemp`,`haerbin_mintemp`) VALUES 
 ('a96a31b3648c4be297d0f00ff5599a9f','1','2016-06-02 21:17:18','1','2016-06-22 00:52:34',NULL,'0','2016-06-22 00:00:00',36,18,36,20,16,20,10,8),
 ('ba1a98106bd44a9ebbfd0b90dd3f89e5','1','2016-06-02 21:21:56','1','2016-06-02 21:21:56',NULL,'0','2016-06-30 00:00:00',24,12,36,18,25,24,12,8),
 ('fa1d274c07b744ee870518e79f817ba6','1','2016-06-02 21:16:54','1','2016-06-02 21:16:54',NULL,'0','2016-06-28 00:00:00',32,12,23,10,60,25,10,2);
/*!40000 ALTER TABLE `test_line_weather_main_city` ENABLE KEYS */;


--
-- Definition of table `test_pie_class`
--

DROP TABLE IF EXISTS `test_pie_class`;
CREATE TABLE `test_pie_class` (
  `id` varchar(64) NOT NULL default '' COMMENT '主键',
  `create_by` varchar(64) default NULL COMMENT '创建者',
  `create_date` datetime default NULL COMMENT '创建时间',
  `update_by` varchar(64) default NULL COMMENT '更新者',
  `update_date` datetime default NULL COMMENT '更新时间',
  `del_flag` varchar(64) default NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `class_name` varchar(64) character set utf8 default NULL COMMENT '班级',
  `num` int(11) default NULL COMMENT '人数',
  `remarks` varchar(255) character set utf8 default NULL COMMENT '备注信息',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='班级';

--
-- Dumping data for table `test_pie_class`
--

/*!40000 ALTER TABLE `test_pie_class` DISABLE KEYS */;
INSERT INTO `test_pie_class` (`id`,`create_by`,`create_date`,`update_by`,`update_date`,`del_flag`,`class_name`,`num`,`remarks`) VALUES 
 ('19141118ea9e46c6b35d8baeb7f3fe94','1','2016-05-26 21:29:26','1','2016-05-26 21:35:06','0','2班',22,'11'),
 ('42b3824ef5dc455e917a3b1f6a8c1108','1','2016-05-26 21:35:26','1','2016-06-02 21:00:10','0','3班',123,''),
 ('49812602ff9445e99219b0d02719dbc1','1','2016-05-26 21:29:33','1','2016-05-26 21:35:00','0','1班',44,'44'),
 ('f5344164632149e199c9c221428f2774','1','2016-06-22 00:52:16','1','2016-06-22 00:52:16','0','4班',4,'4');
/*!40000 ALTER TABLE `test_pie_class` ENABLE KEYS */;

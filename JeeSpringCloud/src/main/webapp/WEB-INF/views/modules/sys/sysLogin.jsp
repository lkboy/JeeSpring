<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<!DOCTYPE html>
<html style="overflow: auto;" class=" js csstransforms3d csstransitions csstransformspreserve3d"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${fns:getConfig('productName')} 登录</title>


<meta name="author" content="http://www.jeespring.org/">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=9,IE=10">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<!-- 移动端禁止缩放事件 -->
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<script src="${ctxStatic}/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/jquery/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/jquery-validation/1.14.0/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctxStatic}/jquery-validation/1.14.0/localization/messages_zh.min.js" type="text/javascript"></script>

	<script src="${ctxStatic}/bootstrap/3.3.4/js/bootstrap.min.js"  type="text/javascript"></script>

<!-- 设置浏览器图标 -->
<link rel="shortcut icon" href="../static/favicon.ico">

<link rel="stylesheet" id="theme" href="${ctxStatic}/common/login/app-midnight-blue.css">
	
	<meta name="decorator" content="ani">
		
		<script>
			if (window.top !== window.self) {
				window.top.location = window.location;
			}
		</script>
		<script type="text/javascript">
				$(document).ready(function() {
					$("#loginForm").validate({
						rules: {
							validateCode: {remote: "/jeespring/servlet/validateCodeServlet"}
						},
						messages: {
							username: {required: "请填写用户名."},password: {required: "请填写密码."},
							validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
						},
						errorLabelContainer: "#messageBox",
						errorPlacement: function(error, element) {
							error.appendTo($("#loginError").parent());
						} 
					});
				});
				// 如果在框架或在对话框中，则弹出提示并跳转到首页
				if(self.frameElement && self.frameElement.tagName == "IFRAME" || $('#left').length > 0 || $('.jbox').length > 0){
					alert('未登录或登录超时。请重新登录，谢谢！');
					top.location = "/admin";
				}
		</script>
	
	
</head>
<body id="" class="" style="">
	
		

		<div class="login-page">
		<div class="row">
			<div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
				<img class="img-circle" src="../static/common/login/images/flat-avatar.png">
				<h1>${fns:getConfig('productName')}</h1>
<!-- 0:隐藏tip, 1隐藏box,不设置显示全部 -->

				<form id="loginForm" role="form" action="${ctx}/login" method="post" novalidate="novalidate">
					<div class="form-content">
						<sys:message content="${message}"/>
						<div class="form-group">
							<input type="text" id="username" name="username" class="form-control input-underline input-lg required" value="admin" placeholder="用户名" aria-required="true">
						</div>

						<div class="form-group">
							<input type="password" id="password" name="password" value="admin" class="form-control input-underline input-lg required" placeholder="密码" aria-required="true">
						</div>
						

						<label class="inline">
								<input type="checkbox" id="rememberMe" name="rememberMe" class="ace">
								<span class="lbl"> 记住我</span>
						</label>
					</div>
					<input type="submit" class="btn btn-white btn-outline btn-lg btn-rounded progress-login" value="登录">
					&nbsp;
					<a href="${ctx}/register" class="btn btn-white btn-outline btn-lg btn-rounded progress-login">注册</a>
				</form>
			</div>			
		</div>
	</div>
	
	<script>

		
$(function(){
		$('.theme-picker').click(function() {
			changeTheme($(this).attr('data-theme'));
		}); 	
	
});

function changeTheme(theme) {
	$('<link>')
	.appendTo('head')
	.attr({type : 'text/css', rel : 'stylesheet'})
	.attr('href', '../static/common/css/app-'+theme+'.css');
	//$.get('api/change-theme?theme='+theme);
	 $.get('../theme/'+theme+'?url='+window.top.location.href,function(result){  });
}
</script>
<style>
li.color-picker i {
    font-size: 24px;
    line-height: 30px;
}
.red-base {
    color: #D24D57;
}
.blue-base {
    color: #3CA2E0;
}
.green-base {
    color: #27ae60;
}
.purple-base {
    color: #957BBD;
}
.midnight-blue-base {
    color: #2c3e50;
}
.lynch-base {
    color: #6C7A89;
}
</style>


</body></html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>首页</title>
    <meta name="decorator" content="default"/>
    <%@ include file="/WEB-INF/views/include/head.jsp"%>
    <script type="text/javascript">
        $(document).ready(function() {
            WinMove();
        });
    </script>
</head>
<body class="gray-bg">
</div>
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-sm-12">
        <blockquote class="text-info" style="font-size:14px">
            ${indexSysConfig.description}
        </blockquote>

        <hr>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-4">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>公告通知</h5>
                    <div style="text-align: center;font-size: 14px;color: #666;">${oaNotify.title}</div><br/>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
                        <ol>
                            <c:forEach items="${pageOaNotify.list}" var="oaNotify">
                                <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                    <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                            </c:forEach>
                        </ol>
                        <hr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>升级日志</h5> <span class="label label-primary">K+</span>
                    <div class="ibox-tools">
                        <!a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="panel-body">
                        <div class="panel-group" id="version">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#version" href="#v1.0"></a><code class="pull-right"></code>
                                    </h5>
                                </div>
                                <div id="v2.0" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <ol>
                                            <c:forEach items="${pageOaNotifyLog.list}" var="oaNotify">
                                                <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                                    <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                                            </c:forEach>
                                        </ol>
                                        <hr>
                                        ${indexTopicsShowList.description}
                                    </div>
                                </div>
                            </div>
                            <!--div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.9</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.8</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.7</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.6</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.5</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.4</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.3</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.2</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.1">v1.1</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.0</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>技术支持</h5>
                </div>
                <div class="ibox-content">
                    <ol>
                        <c:forEach items="${pageOaNotifyTechnology.list}" var="oaNotify">
                            <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                        </c:forEach>
                    </ol>
                    <hr>


                </div>
            </div>
            <!-- <div class="ibox float-e-margins">
               <div class="ibox-title">
                   <h5>联系信息</h5>

               </div>
               <div class="ibox-content">
                   <p><i class="fa fa-send-o"></i> 网址：<a href="http://www.yocity.cn" target="_blank">点我啊</a>
                   </p>

               </div>
           </div>
       </div>-->
            </div-->
        </div>

</body>
</html>
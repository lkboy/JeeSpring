package com.jeespring.modules.gen.dao;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.gen.entity.GenTemplate;

@Mapper
public interface GenTemplateDao
  extends CrudDao<GenTemplate>
{}

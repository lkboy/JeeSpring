package com.jeespring.modules.sys.rest;

import com.jeespring.common.web.BaseController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeespring.modules.sys.entity.Dict;
import com.jeespring.modules.sys.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeespring.common.utils.DateUtils;
import com.jeespring.common.config.Global;
import com.jeespring.common.persistence.Page;
import com.jeespring.common.web.BaseController;
import com.jeespring.common.utils.StringUtils;
import com.jeespring.common.utils.excel.ExportExcel;
import com.jeespring.common.utils.excel.ImportExcel;
import com.jeespring.common.web.Result;
import com.jeespring.common.web.ResultFactory;

@RestController
@RequestMapping(value={"/rest/sys/dict"})
@Api(value="dict数据字典云接口", description="dict数据字典云接口")
public class DictRestController extends BaseController {

    @Autowired
    private DictService dictService;

    @RequestMapping(value = {"get"},method ={RequestMethod.POST,RequestMethod.GET})
    @ApiOperation(value="数据字典获取信息(Content-Type为text/html)", notes="数据字典获取信息(Content-Type为text/html)")
    @ApiImplicitParam(name = "id", value = "数据字典id", required = false, dataType = "String",paramType="query")
    public Result get(@RequestParam(required=false) String id) {
        Dict dict=null;
        if (StringUtils.isNotBlank(id)){
            dict=dictService.get(id);
        }else{
            dict=new Dict();
        }
        Result result = ResultFactory.getSuccessResult();
        result.setResultObject(dict);
        return result;

    }
    @RequestMapping(value = {"get/json"},method ={RequestMethod.POST})
    @ApiOperation(value="数据字典获取信息(Content-Type为application/json)", notes="数据字典获取信息(Content-Type为application/json)")
    @ApiImplicitParam(name = "id", value = "数据字典id", required = false, dataType = "String",paramType="body")
    public Result getJson(@RequestBody(required=false) String id) {
        Dict dict=null;
        if (StringUtils.isNotBlank(id)){
            dict=dictService.get(id);
        }else{
            dict=new Dict();
        }
        Result result = ResultFactory.getSuccessResult();
        result.setResultObject(dict);
        return result;
    }

    //RequiresPermissions("sys:dict:list")
    @RequestMapping(value = {"list"},method ={RequestMethod.POST,RequestMethod.GET})
    @ApiOperation(value="数据字典获取列表(Content-Type为text/html)", notes="数据字典获取列表(Content-Type为text/html)")
    @ApiImplicitParam(name = "dict", value = "数据字典", dataType = "Dict",paramType="query")
    public Result list(Dict dict, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Dict> page = dictService.findPage(new Page<Dict>(request, response), dict);
        Result result = ResultFactory.getSuccessResult();
        result.setResultObject(page);
        return result;
    }

    @RequestMapping(value = {"list/json"},method ={RequestMethod.POST})
    @ApiOperation(value="数据字典获取列表(Content-Type为application/json)", notes="数据字典获取列表(Content-Type为application/json)")
    @ApiImplicitParam(name = "dict", value = "数据字典", dataType = "Dict",paramType="body")
    public Result listJson(@RequestBody Dict dict, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Dict> page = dictService.findPage(new Page<Dict>(request, response), dict);
        Result result = ResultFactory.getSuccessResult();
        result.setResultObject(page);
        return result;
    }

}
